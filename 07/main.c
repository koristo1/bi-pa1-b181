#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <regex.h>

#define STARTING_SIZE 10
#define REALLOC_CONSTANT 2

int comboCounter = 0;

int literalToNumber(const char letter) {
    switch (letter) {
        case 'M':
            return 1000;
        case 'D':
            return 500;
        case 'C':
            return 100;
        case 'L':
            return 50;
        case 'X':
            return 10;
        case 'V':
            return 5;
        case 'I':
            return 1;
        default:
            return -1;
    }
}

int romanToArabic(const char *s) {
    int result = 0;
    int lastValue = INT_MAX;
    int value = 0;

    for (char ch = *s; ch != 0; ch = *(++s)) {
        value = literalToNumber(ch);

        if (value > lastValue)
            result += (value - 2 * lastValue);
        else
            result += value;

        lastValue = value;
    }

    return result;
}


//-----------------------------------------------------------------------------------------------------------

void printOperationsPrompt() {
    printf("Sekvence:\n");
}

void printInvalidInput() {
    printf("Nespravny vstup.\n");
}

void printModulePrompt() {
    printf("Operace:\n");
}

int validPair(const char a, const char b) {
    int x = literalToNumber(a);
    int y = literalToNumber(b);

    return x >= y || x * 5 == y || x * 10 == y;
}

int VLDsequence(const char a, const char b) {
    return a == b && (a == 'V' || a == 'L' || a == 'D');
}

int validRoman(const char *roman) {
    char expression[] = "^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$";
    regex_t regex;
    int reti = regcomp(&regex, expression, REG_EXTENDED);
    if (reti) {
        printf("Could not compile regex.\n");
        exit(1);
    }

    reti = regexec(&regex, roman, 0, NULL, 0);

    regfree(&regex);

    if (!reti)
        return 1;
    if (reti == REG_NOMATCH)
        return 0;

    return -1;
}

int validOperand(const char *operand, size_t size) {

    //single operand, doesn't break any rules
    if (size == 1)
        return 1;

    int sequenceCounter = 0;
    char lastRead = operand[0];

    if (!validRoman(operand))
        return 0;

    for (size_t i = 1; i < size; i++) {

        sequenceCounter = (lastRead == operand[i]) ? sequenceCounter + 1 : 0;

        if (VLDsequence(lastRead, operand[i]) || sequenceCounter == 3 || !validPair(lastRead, operand[i]))
            return 0;

        lastRead = operand[i];
    }

    return 1;
}

char *findPrefix(const char *operands, size_t i) {

    char *prefix = (char *) malloc((i + 1) * sizeof(char));

    for (size_t j = 0; j < i; ++j) {
        prefix[j] = operands[j];
    }
    prefix[i] = '\0';

    return prefix;
}

void findSolution(const char *operands, size_t operandsSize, char *output, size_t outputIndex, int module, int sum,
                  int *originalSum) {

    if (operandsSize == 0 && sum == 0) { //last element

        if (module == 1) {
            size_t i = 0;

            printf("> %d = ", *originalSum);

            while (output[i] != '\0')
                printf("%c", output[i++]);
            printf("\n");
        }
        comboCounter++;
        return;
    }


    for (size_t prefixSize = 1; prefixSize <= operandsSize; prefixSize++) {
        char *prefix = findPrefix(operands, prefixSize);
        if (prefix == NULL)
            return;

        if (!validOperand(prefix, prefixSize)) {
            free(prefix);
            return;
        }

        int prefixValue = romanToArabic(prefix);

        output[outputIndex] = '+';
        outputIndex++;

        for (size_t i = 0; i < prefixSize; i++) {
            output[outputIndex] = prefix[i];
            outputIndex++;
        }

        output[outputIndex] = '\0';
        outputIndex = outputIndex - prefixSize - 1;

        free(prefix);

        if(sum - prefixValue < 0)
            return;

        if (sum - prefixValue >= 0)
            findSolution(operands + prefixSize, operandsSize - prefixSize, output, outputIndex + prefixSize + 1, module,
                         sum - prefixValue, originalSum);
    }
}

void recursionSetup(const char *operands, size_t operandsSize, int module, int sum, int *originalSum) {
    size_t outputSize = 2 * operandsSize; //n operands plus up to (worst case scenario) n-1 plus signs
    size_t outputIndex = 0;
    char *output = (char *) malloc(outputSize * sizeof(char));

    if (output == NULL)
        return;

    comboCounter = 0;

    for (size_t prefixSize = 1; prefixSize <= operandsSize; prefixSize++) {
        char *prefix = findPrefix(operands, prefixSize);
        if (prefix == NULL) {
            free(output);
            return;
        }

        if (!validOperand(prefix, prefixSize)) {
            free(prefix);
            continue;
        }

        int prefixValue = romanToArabic(prefix);

        for (size_t i = 0; i < prefixSize; i++) {
            output[outputIndex] = prefix[i];
            outputIndex++;
        }

        output[outputIndex] = '\0';

        outputIndex = outputIndex - prefixSize;

        free(prefix);

        if(sum - prefixValue < 0)
            continue;

        if (sum - prefixValue >= 0)
            findSolution(operands + prefixSize, operandsSize - prefixSize, output, outputIndex + prefixSize, module,
                         sum - prefixValue, originalSum);
    }

    free(output);
    printf("%d: %d\n", sum, comboCounter);
}


int isRoman(char c) {
    char numerals[] = "IVXLCDM";
    size_t size = strlen(numerals);

    for (size_t i = 0; i < size; i++)
        if (c == numerals[i])
            return 1;

    return 0;
}

char *inputOperands(size_t *size) {

    printOperationsPrompt();

    char input = ' ';

    int maximalSize = STARTING_SIZE;
    int currentSize = 0;
    int emptyFlag = 1;
    char *operands = (char *) malloc((STARTING_SIZE) * sizeof(char));

    if (operands == NULL)
        return NULL;

    while (scanf("%c", &input) == 1 && input != '\n') {
        emptyFlag = 0;
        if (!isRoman(input)) {
            free(operands);
            return NULL;
        }

        //stretchString
        if (maximalSize == currentSize) {
            maximalSize *= REALLOC_CONSTANT;
            operands = (char *) realloc(operands, maximalSize * sizeof(char));
            if (operands == NULL)
                return NULL;
        }


        operands[currentSize] = input;
        currentSize++;
    }

    if(emptyFlag)
        return NULL;


    //shrink string
    operands = (char *) realloc(operands, (currentSize + 1) * sizeof(char));
    operands[currentSize] = '\0';
    *size = currentSize;
    return operands;
}

int inputModule() {

    const char list[] = "list";
    const char count[] = "count";
    int listIdentity = 0;
    int countIdentity = 0;
    char input = ' ';
    int validInput = 0;


    while ((validInput = scanf(" %c", &input))) {

        if (validInput == EOF)
            return -1;

        if (validInput != 1)
            return 0;

        if (input == list[listIdentity])
            listIdentity++;
        else if (input == count[countIdentity])
            countIdentity++;
        else return 0;

        if (listIdentity == 4)
            return 1;

        if (countIdentity == 5)
            return 2;

    }

    return 0;
}

int inputResult() {

    int input = 0;

    if (scanf("%d", &input) != 1 || input <= 0)
        return 0;

    return input;
}

int main() {


    size_t operandsSize = 0;
    char *operands = inputOperands(&operandsSize);

    if (operands == NULL) {
        printInvalidInput();
        return 1;
    }

    printModulePrompt();

    while (1) {

        int module = inputModule();
        //test this
        if (module == EOF)
            break;

        if (!module) {
            printInvalidInput();
            free(operands);
            return 1;
        }

        int result = inputResult();
        if (!result) {
            printInvalidInput();
            free(operands);
            return 1;
        }

        recursionSetup(operands, operandsSize, module, result, &result);

    }

    free(operands);
    return 1;
}