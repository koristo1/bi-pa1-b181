#include <stdio.h>
#include <stdlib.h>

//-------------------------------------------------
#define STARTING_SIZE 2
#define REALLOC_CONSTANT 2



//comparing chars for input------------------------
#define LEFT_CURLY '{'
#define RIGHT_CURLY '}'
#define LEFT_SQUARE '['
#define RIGHT_SQUARE ']'
#define COMMA ','
#define COLON ':'
#define EQUALS_SIGN '='
//-------------------------------------------------
enum tollIndex {
    A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z
};
static char toll[] = {"ABCDEFGHIJKLMNOPQRSTUVWXYZ"};

//-------------------------------------------------
typedef struct Segment {
    int length;
    int distanceFromStart;
    double tolls[26];
} segment;

//function declarations----------------------------
void printDataQuery();

void printInputQuery();

void printInvalidInput();

void printResult(segment reference, int from, int to, int swapped);

void swap(int *a, int *b);

void addTolls(segment *reference, int distance, double *tolls);

void saveToArray(segment *array, int currentIndex, segment reference);

void updateLength(int currentLength, int *totalLength);

int checkToll(char challenger);

int constraints(int *from, int *to, int totalLength, int *swapped);

int loadTolls(segment **array, int *currentIndex, int *maximalSize, segment *reference, int *totalLength);

int loadData(segment **array, int *currentSize, int *length);

int serveInput(segment *array, int currentSize, int totalLength);

void updateDistance(segment *reference);

segment referenceSetup();

segment *stretchArray(segment *array, int currentSize, int *maximalSize);

//-------------------------------------------------
int main() {
    segment *array = (segment *) malloc(sizeof(*array) * STARTING_SIZE);
    int currentSize = 0;
    int length = 0;

    if (array == NULL) {
        free(array);
        printInvalidInput();
        return 1;
    }

    if (!loadData(&array, &currentSize, &length)) {
        free(array);
        printInvalidInput();
        return 1;
    }

    if (!serveInput(array, currentSize, length)) {
        free(array);
        printInvalidInput();
        return 1;
    }

    free(array);
    return 0;
}

//data-input---------------------------------------
int loadData(segment **array, int *currentSize, int *length) {
    char c = ' ';
    int maximalSize = STARTING_SIZE;

    printDataQuery();
    if (scanf(" %c", &c) != 1 || c != LEFT_CURLY)
        return 0;

    segment reference = referenceSetup();

    while (1) {

        //opening bracket
        if (scanf(" %c", &c) != 1 || c != LEFT_SQUARE)
            return 0;

        //individual tolls
        if (!loadTolls(array, currentSize, &maximalSize, &reference, length))
            return 0;

        if (scanf(" %c", &c) != 1)
            return 0;

        //End of data input
        if (c == RIGHT_CURLY)
            return 1;

        //no ending bracket and no delimiter
        if (c != COMMA)
            return 0;
    }
}

int loadTolls(segment **array, int *currentIndex, int *maximalSize, segment *reference, int *totalLength) {
    char c = ' ';

    //length
    if (scanf("%d", &(reference->length)) != 1 || reference->length <= 0)
        return 0;

    //check for boundaries and add to the maximal length
    updateLength(reference->length, totalLength);

    if (scanf(" %c", &c) != 1 || c != COLON)
        return 0;

    while (1) {

        //valid toll type
        if (scanf(" %c", &c) != 1)
            return 0;

        int tollIndex = checkToll(c);

        if (tollIndex == -1)
            return 0;

        //equation symbol
        if (scanf(" %c", &c) != 1 || c != EQUALS_SIGN)
            return 0;

        //overwrite the value at given index
        if (scanf("%lf", &reference->tolls[tollIndex]) != 1 || reference->tolls[tollIndex] < 0)
            return 0;

        //trailing char
        if (scanf(" %c", &c) != 1)
            return 1;

        //end of current segment
        if (c == RIGHT_SQUARE) {
            //stretch if neccessary
            if (*currentIndex + 1 == *maximalSize) {
                *maximalSize = (*maximalSize) * REALLOC_CONSTANT;
                *array = (segment *) realloc(*array, *maximalSize * sizeof(segment));
            }

            //check realloc's success
            if (*array == NULL)
                return 1;


            //insert the value and update the size
            updateDistance(reference);
            saveToArray(*array, *currentIndex, *reference);
            *currentIndex = *currentIndex + 1;
            return 1;
        }

        //invalid format, no RIGHT_SQUARE or comma to separate tolls
        if (c != COMMA)
            return 0;
    }
}

int checkToll(char challenger) {
    /*Checks the char against the tolls (letters of the standard alphabet)
      Returns -1 if no match has been found
      Otherwise returns the index of the letter*/

    for (int i = A; i <= Z; i++)
        if (toll[i] == challenger)
            return i;

    return -1;
}

segment *stretchArray(segment *array, int currentSize, int *maximalSize) {
    //checks whether the array has been filled up and reallocates a larger memory block if neccessary
    if (currentSize < *maximalSize)
        return array;

    *maximalSize = (*maximalSize) * REALLOC_CONSTANT;

    return (segment *) realloc(array, *maximalSize * sizeof(array[0]));
}

void saveToArray(segment *array, int currentIndex, segment reference) {
    array[currentIndex].length = reference.length;
    array[currentIndex].distanceFromStart = reference.distanceFromStart;

    for (int i = A; i <= Z; i++)
        array[currentIndex].tolls[i] = reference.tolls[i];
}

//data-operations----------------------------------
int serveInput(segment *array, int currentSize, int totalLength) {
    int from = 0;
    int to = 0;
    int input = 0;

    printInputQuery();

    while (1) {
        //creates a dummy to save the tolls into and sets it to 0 values
        segment dummy = referenceSetup();

        int swapped = 0;
        input = constraints(&from, &to, totalLength, &swapped);
        if (input == EOF) //check EOF
            return 1;
        if (!input) //check validity
            return 0;

        int current = 0;
        int entered = 1;
        //find first segment
        for (int i = 0; i < currentSize; i++) {
            if (array[i].distanceFromStart > from) {
                current = i;
                break;
            }
        }
        int length = 0;

        for (int i = current; i < currentSize; i++) {
            if (to < array[i].distanceFromStart) //found the ending point
            {
                length = i == current ? to - from : to - array[i - 1].distanceFromStart;
                addTolls(&dummy, length, array[i].tolls);
                break;
            }

            //get the remaining length spent in the current segment if the highway has been entered in this segment, otherwise get the length of the segment

            if (entered && from < array[i].distanceFromStart) {
                length = array[i].distanceFromStart - from; //found the starting point in the current segment
                entered = 0;
            } else
                length = array[i].distanceFromStart - array[i - 1].distanceFromStart;


            addTolls(&dummy, length, array[i].tolls);
        }

        printResult(dummy, from, to, swapped);
    }
}

int constraints(int *from, int *to, int totalLength, int *swapped) {
    int input = scanf("%d", from);

    if (input == -1)
        return -1;

    if (input != 1 || *from < 0 || *from > totalLength)
        return 0;

    if (scanf("%d", to) != 1 || *to < 0 || *to > totalLength || *to == *from)
        return 0;

    if (*from > *to) {
        *swapped = 1;
        swap(from, to);
    }

    return 1;
}

void swap(int *a, int *b) {
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

void addTolls(segment *reference, int distance, double *tolls) {
    for (int i = A; i <= Z; i++)
        reference->tolls[i] += tolls[i] * distance;
}

//segment-manipulation-----------------------------
segment referenceSetup() {
    //setup the starting reference, with all zeros
    segment reference;
    reference.length = 0;
    reference.distanceFromStart = 0;
    
    for (int i = A; i <= Z; i++)
        reference.tolls[i] = 0;

    return reference;
}

void updateDistance(segment *reference) {
    reference->distanceFromStart = reference->distanceFromStart + reference->length;
}

void updateLength(int currentLength, int *totalLength) {
    *totalLength = *totalLength + currentLength;
}

//prints-------------------------------------------
void printDataQuery() {
    printf("Myto:\n");
}

void printInputQuery() {
    printf("Hledani:\n");
}

void printInvalidInput() {
    printf("Nespravny vstup.\n");
}

void printTolls(segment reference, int from, int to) {

    printf("%d - %d: ", from, to);

    for (int i = A; i <= Z; i++) {
        printf("%c=%f", toll[i], reference.tolls[i]);
        if (i != Z)
            printf(", ");
    }

    printf("\n");
}

void printResult(segment reference, int from, int to, int swapped) {

    if (swapped)
        swap(&from, &to);

    printf("%d - %d:", from, to);

    int first = 1;
    for (int i = A; i <= Z; i++) {
        if (reference.tolls[i] > 0) {
            if (!first)
                printf(", ");
            if (first) {
                printf(" ");
                first = 0;
            }

            printf("%c=%f", toll[i], reference.tolls[i]);
        }
    }

    printf("\n");
}
