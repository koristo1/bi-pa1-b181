#include <stdio.h>

enum mod { LENGTH, ZEROS, SEQUENCE };

void parseArray(int low, int high, int mod)
{
	int counterZeros = 0;
	int totalSize = 0;
	int consequentZeros = 0;
	int currentZeros = 0;
	int n;

	if (low == 0 && high == 0)
	{
		totalSize++;
		consequentZeros++;
		currentZeros++;
	}

	for (int i = low; i <= high; i++)
	{
		n = i;
		if (n == 0)
		{
			totalSize++;
			currentZeros++;
			counterZeros++;
		}

		while (n > 0)
		{

			if (n % 2 == 1)
			{
				if (consequentZeros < currentZeros)
					consequentZeros = currentZeros;
				currentZeros = 0;
			}
			else
			{
				currentZeros++;
				counterZeros++;
			}
			totalSize++;
			n /= 2;
		}
	}

	if (mod == LENGTH)
		printf("Cifer: %d\n", totalSize);
	if (mod == ZEROS)
		printf("Nul: %d\n", counterZeros);
	if (mod == SEQUENCE)
		printf("Sekvence: %d\n", consequentZeros);

}

void inputPrompt() {
	printf("Zadejte interval:\n");
}

int input(int * low, int * high, int * mod) {
	char c;
	inputPrompt();
	int success = 1;
	success &= scanf(" %c", &c) == 1 && c == '<';
	success &= scanf(" %d", low) == 1 && *low >= 0;
	success &= scanf(" %c", &c) == 1 && c == ';';
	success &= scanf(" %d", high) == 1 && *low <= *high;
	success &= scanf(" %c", &c) == 1 && c == '>';
	success &= scanf(" %c", &c) == 1;
	success &= (c == 'l' || c == 's' || c == 'z');

	if (!success)
		return 0;

	if (c == 'l')
		*mod = LENGTH;
	if (c == 's')
		*mod = SEQUENCE;
	if (c == 'z')
		*mod = ZEROS;

	return 1;
}

int main() {
	int low = 0;
	int high = 0;
	int mod = 0;
	if (!input(&low, &high, &mod))
	{
		printf("Nespravny vstup.\n");
		return 0;
	}

	parseArray(low, high, mod);
	return 0;
}
