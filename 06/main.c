#ifndef __PROGTEST__

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <assert.h>

#endif /* __PROGTEST__ */

//--------------------------------------------------------
#define REALLOC_MULTIPLIER 2
#define OUTPUT_STARTING_SIZE 8
#define TOKEN_STARTING_SIZE 4

//--------------------------------------------------------
#define SPACE ' '
#define TABULATOR '\t'
#define NEW_LINE '\n'
#define TERMINATOR '\0'

//function declaration------------------------------------
int isWhiteChar(const char source);

int isNewLine(const char source);

void skipWhiteChars(const char *source, int *index);

//--------------------------------------------------------
int isNewLine(const char source) {
    return source == NEW_LINE;
}

void skipWhiteChars(const char *source, int *index) {
    int current = *index;

    while (source[current] != TERMINATOR && isWhiteChar(source[current]))
        current++;

    *index = current;
}

char *stretchArray(int length, int *size, char *array) {
    if (length < *size)
        return array;

    while (length >= *size)
        *size = *size * REALLOC_MULTIPLIER;

    (*size) += 2;

    array = (char *) realloc(array, sizeof(array) * *size);

    return array;
}

char *tokenise(const char *src, int *startingIndex, int *tokenLength) {
    *tokenLength = 0;
    int currentIndex = *startingIndex;
    int tokenSize = TOKEN_STARTING_SIZE;
    char *token = (char *) malloc(sizeof(token) * tokenSize);

    if (token == NULL)
        return NULL;

    while (src[currentIndex] != TERMINATOR && !isWhiteChar(src[currentIndex])) {
        token = stretchArray(*tokenLength, &tokenSize, token);

        if (token == NULL)
            return NULL;

        token[*tokenLength] = src[currentIndex];
        (*tokenLength)++;
        currentIndex++;
    }

    //Append a white char. Check array size and realloc's success
    token = stretchArray(*tokenLength, &tokenSize, token);
    if (token == NULL)
        return NULL;

    token[*tokenLength] = SPACE;
    (*tokenLength)++;

    *startingIndex = currentIndex;
    return token;

}

int wordFitsOnLine(int wordLength, int lineWidth) {
    return wordLength - 1 <= lineWidth; // remove the trailing white char from the equation
}

void append(char *output, const char *token, int *outputLength, int tokenLength) {
    //append the token

    int length = *outputLength;

    for (int i = 0; i < tokenLength; i++)
        output[length++] = token[i];

    *outputLength = length;
}

int readParagraph(const char *src, int index) {

    int newlineCounter = 0;

    while (src[index] != TERMINATOR && isNewLine(src[index])) {
        newlineCounter++;
        index++;
    }

    return newlineCounter >= 2;
}

void replaceEnding(char *string, int *index, int *currentWidth) {
    string[(*index) - 1] = NEW_LINE;
    string[(*index)] = NEW_LINE;
    (*index)++;
    *currentWidth = 0;
}

char *wordWrap(int width, const char *src) {


    //setup
    int sourceAt = 0;
    int outputSize = OUTPUT_STARTING_SIZE;
    int outputLength = 0;
    int tokenLength = 0;
    int currentWidth = 0;
    char *output = (char *) malloc(sizeof(output) * outputSize);
    int paragraphFlag = 0;

    //check malloc's success
    if (output == NULL)
        return NULL;

    if (src[0] == TERMINATOR) {
        return output;
    }


    while (src[sourceAt] != TERMINATOR) {
        skipWhiteChars(src, &sourceAt);

        char *token = tokenise(src, &sourceAt, &tokenLength);
        if (token == NULL) {
            free(output);
            return NULL;
        }

        if (tokenLength == 1) {
            free(output);
            token[0] = TERMINATOR;
            return token;

        }

        //check whether the current word fits in a single row
        if (!wordFitsOnLine(tokenLength, width)) {
            free(output);
            free(token);
            return NULL;
        }

        paragraphFlag = readParagraph(src, sourceAt);

        output = stretchArray(outputLength + tokenLength + paragraphFlag, &outputSize, output);
        if (output == NULL) {
            free(token);
            return NULL;
        }

        if (currentWidth + tokenLength - 1 > width /*ignore the last white char*/) {
            //word cannot fit with the rest of the line, need to put on a new line
            output[outputLength - 1] = NEW_LINE;
            currentWidth = tokenLength; //newline, reset
        } else {
            currentWidth += tokenLength;
        }

        append(output, token, &outputLength, tokenLength);
        if (paragraphFlag == 1)
            replaceEnding(output, &outputLength, &currentWidth);

        free(token);
        skipWhiteChars(src, &sourceAt);
    }

    if (paragraphFlag)
        output[outputLength - 1] = TERMINATOR;
    else
        output[outputLength - 1] = NEW_LINE; //replace the last space with terminator, SHRINK

    outputSize = outputLength;
    output = (char *) realloc(output, sizeof(output) * outputSize);
    output[outputLength] = TERMINATOR;

    return output;
}

int isWhiteChar(const char source) {
    return source == SPACE || source == TABULATOR || source == NEW_LINE;
}


#ifndef __PROGTEST__

int main(void) {
    char *res;
    const char *s0 =
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer metus\n"
            "pede, pretium vitae, rhoncus et, auctor sit amet, ligula. Integer volutpat\n"
            "orci et elit. Nunc tempus, urna at sollicitudin rutrum, arcu libero rhoncus\n"
            "lectus, vitae feugiat purus orci ultricies turpis. Pellentesque habitant\n"
            "morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nam\n"
            "in pede. Etiam eu sem id urna ultricies congue. Vestibulum porttitor\n"
            "ultrices neque. Mauris semper, mauris ut feugiat ultricies, augue purus\n"
            "tincidunt  elit, eu interdum ante nisl ac ante. Pellentesque dui. Vestibulum\n"
            "pretium, augue non cursus pretium, nibh dolor laoreet leo, sed pharetra pede\n"
            "libero non diam.";
    const char *s1 =
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer metus\n"
            "pede, pretium vitae, rhoncus et, auctor sit amet, ligula. Integer volutpat\n"
            "orci et elit. Nunc tempus, urna at sollicitudin rutrum, arcu libero rhoncus\n"
            "lectus, vitae feugiat purus orci ultricies turpis. Pellentesque habitant\n"
            "morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nam\n"
            "in pede. Etiam eu sem id urna ultricies congue. Vestibulum porttitor\n"
            "ultrices neque. Mauris semper, mauris ut feugiat ultricies, augue purus\n"
            "tincidunt  elit, eu interdum ante nisl ac ante. Pellentesque dui. Vestibulum\n"
            "pretium, augue non cursus pretium, nibh\tdolor laoreet leo, sed pharetra pede\n"
            "libero non diam.\n"
            "\n"
            "Proin est nisi,                     gravida ac, vulputate id, fringilla sit\n"
            "amet, magna. Nam congue cursus magna. In malesuada, velit a gravida sodales,\n"
            "dolor nisl vestibulum orci, sit amet sagittis mauris tellus nec purus. Nulla\n"
            "eget risus. Quisque nec sapien blandit odio convallis ullamcorper. Lorem\n"
            "ipsum dolor sit amet, consectetuer adipiscing elit. Pellentesque cursus.\n"
            "Aliquam tempus neque vitae libero molestie ut auctor.\n"
            "\n"
            "\n"
            "\n"
            "In nec massa eu tortor vulputate suscipit.\tNam tristique magna nec pede. Sed\n"
            "a nisi. Nulla sed augue ut risus placerat porttitor. Ut aliquam. Nulla\n"
            "facilisi. Nulla vehicula nibh ac sapien. Nunc facilisis dapibus ipsum. Donec\n"
            "sed mauris. Nulla quam nisi, laoreet non, dignissim posuere, lacinia nec,\n"
            "turpis. Mauris malesuada nisi sed enim. In hac habitasse platea dictumst.\n"
            "Fusce    faucibus, turpis nec auctor posuere, nulla tellus scelerisque metus,\n"
            "quis molestie mi dui id quam. Mauris vestibulum. Nam ullamcorper.\n"
            "\n";


    res = wordWrap(40, "");
    assert(!strcmp(res, ""));
    free(res);

    res = wordWrap(50, "abc          def         ghi        jkl   mno         pqr          stu\t\t\t\t");
    assert(!strcmp(res, "abc def ghi jkl mno pqr stu\n"));

    free(res);

    res = wordWrap(40, "   \n\n\n\t\t\t");
    assert(!strcmp(res, ""));

    free(res);

    res = wordWrap(40, "\n\nn\t\t  ");
    assert(!strcmp(res, "n\n"));
    free(res);


    res = wordWrap(40, s0);
    assert (!strcmp(res,
                    "Lorem ipsum dolor sit amet, consectetuer\n"
                    "adipiscing elit. Integer metus pede,\n"
                    "pretium vitae, rhoncus et, auctor sit\n"
                    "amet, ligula. Integer volutpat orci et\n"
                    "elit. Nunc tempus, urna at sollicitudin\n"
                    "rutrum, arcu libero rhoncus lectus,\n"
                    "vitae feugiat purus orci ultricies\n"
                    "turpis. Pellentesque habitant morbi\n"
                    "tristique senectus et netus et malesuada\n"
                    "fames ac turpis egestas. Nam in pede.\n"
                    "Etiam eu sem id urna ultricies congue.\n"
                    "Vestibulum porttitor ultrices neque.\n"
                    "Mauris semper, mauris ut feugiat\n"
                    "ultricies, augue purus tincidunt elit,\n"
                    "eu interdum ante nisl ac ante.\n"
                    "Pellentesque dui. Vestibulum pretium,\n"
                    "augue non cursus pretium, nibh dolor\n"
                    "laoreet leo, sed pharetra pede libero\n"
                    "non diam.\n"));
    free(res);

    res = wordWrap(120, s0);
    assert (!strcmp(res,
                    "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer metus pede, pretium vitae, rhoncus et, auctor sit\n"
                    "amet, ligula. Integer volutpat orci et elit. Nunc tempus, urna at sollicitudin rutrum, arcu libero rhoncus lectus, vitae\n"
                    "feugiat purus orci ultricies turpis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac\n"
                    "turpis egestas. Nam in pede. Etiam eu sem id urna ultricies congue. Vestibulum porttitor ultrices neque. Mauris semper,\n"
                    "mauris ut feugiat ultricies, augue purus tincidunt elit, eu interdum ante nisl ac ante. Pellentesque dui. Vestibulum\n"
                    "pretium, augue non cursus pretium, nibh dolor laoreet leo, sed pharetra pede libero non diam.\n"));
    free(res);

    res = wordWrap(10, s1);
    assert (res == NULL);

    res = wordWrap(40, s1);
    assert (!strcmp(res,
                    "Lorem ipsum dolor sit amet, consectetuer\n"
                    "adipiscing elit. Integer metus pede,\n"
                    "pretium vitae, rhoncus et, auctor sit\n"
                    "amet, ligula. Integer volutpat orci et\n"
                    "elit. Nunc tempus, urna at sollicitudin\n"
                    "rutrum, arcu libero rhoncus lectus,\n"
                    "vitae feugiat purus orci ultricies\n"
                    "turpis. Pellentesque habitant morbi\n"
                    "tristique senectus et netus et malesuada\n"
                    "fames ac turpis egestas. Nam in pede.\n"
                    "Etiam eu sem id urna ultricies congue.\n"
                    "Vestibulum porttitor ultrices neque.\n"
                    "Mauris semper, mauris ut feugiat\n"
                    "ultricies, augue purus tincidunt elit,\n"
                    "eu interdum ante nisl ac ante.\n"
                    "Pellentesque dui. Vestibulum pretium,\n"
                    "augue non cursus pretium, nibh dolor\n"
                    "laoreet leo, sed pharetra pede libero\n"
                    "non diam.\n"
                    "\n"
                    "Proin est nisi, gravida ac, vulputate\n"
                    "id, fringilla sit amet, magna. Nam\n"
                    "congue cursus magna. In malesuada, velit\n"
                    "a gravida sodales, dolor nisl vestibulum\n"
                    "orci, sit amet sagittis mauris tellus\n"
                    "nec purus. Nulla eget risus. Quisque nec\n"
                    "sapien blandit odio convallis\n"
                    "ullamcorper. Lorem ipsum dolor sit amet,\n"
                    "consectetuer adipiscing elit.\n"
                    "Pellentesque cursus. Aliquam tempus\n"
                    "neque vitae libero molestie ut auctor.\n"
                    "\n"
                    "In nec massa eu tortor vulputate\n"
                    "suscipit. Nam tristique magna nec pede.\n"
                    "Sed a nisi. Nulla sed augue ut risus\n"
                    "placerat porttitor. Ut aliquam. Nulla\n"
                    "facilisi. Nulla vehicula nibh ac sapien.\n"
                    "Nunc facilisis dapibus ipsum. Donec sed\n"
                    "mauris. Nulla quam nisi, laoreet non,\n"
                    "dignissim posuere, lacinia nec, turpis.\n"
                    "Mauris malesuada nisi sed enim. In hac\n"
                    "habitasse platea dictumst. Fusce\n"
                    "faucibus, turpis nec auctor posuere,\n"
                    "nulla tellus scelerisque metus, quis\n"
                    "molestie mi dui id quam. Mauris\n"
                    "vestibulum. Nam ullamcorper.\n"));
    free(res);

    res = wordWrap(80, s1);
    assert (!strcmp(res,
                    "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer metus pede,\n"
                    "pretium vitae, rhoncus et, auctor sit amet, ligula. Integer volutpat orci et\n"
                    "elit. Nunc tempus, urna at sollicitudin rutrum, arcu libero rhoncus lectus,\n"
                    "vitae feugiat purus orci ultricies turpis. Pellentesque habitant morbi tristique\n"
                    "senectus et netus et malesuada fames ac turpis egestas. Nam in pede. Etiam eu\n"
                    "sem id urna ultricies congue. Vestibulum porttitor ultrices neque. Mauris\n"
                    "semper, mauris ut feugiat ultricies, augue purus tincidunt elit, eu interdum\n"
                    "ante nisl ac ante. Pellentesque dui. Vestibulum pretium, augue non cursus\n"
                    "pretium, nibh dolor laoreet leo, sed pharetra pede libero non diam.\n"
                    "\n"
                    "Proin est nisi, gravida ac, vulputate id, fringilla sit amet, magna. Nam congue\n"
                    "cursus magna. In malesuada, velit a gravida sodales, dolor nisl vestibulum orci,\n"
                    "sit amet sagittis mauris tellus nec purus. Nulla eget risus. Quisque nec sapien\n"
                    "blandit odio convallis ullamcorper. Lorem ipsum dolor sit amet, consectetuer\n"
                    "adipiscing elit. Pellentesque cursus. Aliquam tempus neque vitae libero molestie\n"
                    "ut auctor.\n"
                    "\n"
                    "In nec massa eu tortor vulputate suscipit. Nam tristique magna nec pede. Sed a\n"
                    "nisi. Nulla sed augue ut risus placerat porttitor. Ut aliquam. Nulla facilisi.\n"
                    "Nulla vehicula nibh ac sapien. Nunc facilisis dapibus ipsum. Donec sed mauris.\n"
                    "Nulla quam nisi, laoreet non, dignissim posuere, lacinia nec, turpis. Mauris\n"
                    "malesuada nisi sed enim. In hac habitasse platea dictumst. Fusce faucibus,\n"
                    "turpis nec auctor posuere, nulla tellus scelerisque metus, quis molestie mi dui\n"
                    "id quam. Mauris vestibulum. Nam ullamcorper.\n"));
    free(res);

    return 0;
}

#endif /* __PROGTEST__ */
