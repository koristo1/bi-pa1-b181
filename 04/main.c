#include <stdio.h>
#include <stdlib.h>
#define SIZE 1000000

int locations[SIZE];

void printArray(int * array, int size) {
	for (int i = 0; i < size; i++)
		printf("%d <=", array[i]);

	printf("\n");
}

int comparator(const void *a, const void *b) {
	return (*(int*)a - *(int*)b);
}

void printDataQuery()
{
	printf("Mozna umisteni:\n");
}
void printInputQuery() {
	printf("Vzdalenosti:\n");
}

int loadData(int * locations, int  * length, int * locationsSize) {
	printDataQuery();
	char c = 0;
	int input = 0;

	//length
	if (scanf(" %d", &input) != 1 || input <= 0)
		return 0;
	*length = input;

	//colon
	if (scanf(" %c", &c) != 1 || c != ':')
		return 0;

	//opening curly bracket
	if (scanf(" %c", &c) != 1 || c != '{')
		return 0;

	int i = 0;

	while (scanf("%d %c", &input, &c) == 2) {

		//check interval <1;length-1>
		//negative or zero distance
		if (input >= *length || i > SIZE || input < 1)
			return 0;
		locations[i++] = input;

		if (c != ',' && c != '}')
			return 0;

		if (c == '}')
			break;
	}

	if (i == 0) //no positions
		return 0;

	*locationsSize = i;
	return 1;
}
/**
* returns 0 if next element is out of bounds (i.e if previous one was last)
* returns 1 otherwise
**/
int biggestInRange(int distance, int highestIndex, int * last, int *lastViable, int * isCounting, int * i, int * j, int * testedPoint, int * locations) {
	while (locations[*j] - *last <= distance)
	{
		*lastViable = locations[*j];
		*i = *j;
		*isCounting = 0;
		*j = *j + 1;
		if (*j > highestIndex) //last element
			return 0;
	}
	*i = *j - 1;
	*lastViable = locations[*i];
	*last = *lastViable;
	*testedPoint = locations[*j];
	return 1;
}


int analyzeMotorway(int  distance, int maximum, int size, int * locations) {
	int i = 0;
	int j = 0;
	int last = 0;
	int lastViable = 0;
	int billboardCounter = 0;
	//int distance = distance
	//int end = maximum
	int testedPoint = locations[0];
	int isCounting = 0;
	int highestIndex = size - 1;
	int status = 0;

	if (distance >= maximum)
		return 0;


	while (1)
	{
		if (!(testedPoint - last <= distance))
			return -1;


		isCounting = 1;


		if (isCounting)
			billboardCounter++;
		j++;

		if (j > highestIndex) //last element
			break;

		status = biggestInRange(distance, highestIndex, &last, &lastViable, &isCounting, &i, &j, &testedPoint, locations);

		if (status == 0)
			break;

		if (status == 1 && maximum - lastViable <= distance)
			return billboardCounter;

	}

	i = j - 1;

	if (maximum - locations[i] <= distance) //is close enough to the end point
		return billboardCounter;
	else
		return -1;

	return 1;
}

int calculateBillboards(int length, int locationsSize, int * locations) {
	printInputQuery();
	int input = 0;
	int status = 0;
	int result = 0;

	do {
		status = scanf("%d", &input);
		if (status == EOF)
			break;
		if (status != 1 || input <= 0 )
			return 0;
		result = analyzeMotorway(input, length, locationsSize, locations);
		result != -1 ? printf("Billboardu: %d", result) : printf("N/A");
		printf("\n");

	}

	while (1);

	return 1;
}

int main() {
	int length = 0;
	int locationsSize = 0;

	if (!loadData(locations, &length, &locationsSize))
	{
		printf("Nespravny vstup.\n");
		return 1;
	}

	qsort(locations, locationsSize, sizeof(int), comparator);
	if(!calculateBillboards(length, locationsSize, locations))
	{
		printf("Nespravny vstup.\n");
		return 1;
	}


	return 0;
}

