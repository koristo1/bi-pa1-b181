#include <stdio.h>
#include <string.h>

enum mod { LENGTH, ZEROS, SEQUENCE };

void inputPrompt() {
	printf("Zadejte intervaly:\n");
}

void invalidInput() {
	printf("Nespravny vstup.\n");
}

void printResult(int mod, int totalSize, int totalZero, int consequentZero) {
	switch (mod)
	{
	case LENGTH:
		printf("Cifer: %d\n", totalSize);
		break;
	case ZEROS:
		printf("Nul: %d\n", totalZero);
		break;
	case SEQUENCE:
		printf("Sekvence: %d\n", consequentZero);
		break;
	};
}

void parseArray(int low, int high, int mod, int base)
{
	int totalZero = 0;
	int totalSize = 0;
	int consequentZero = 0;
	int currentZeros = 0;
	int remainder;
	int n;

	if (low == 0 && high == 0)
	{
		totalSize++;
		consequentZero++;
		currentZeros++;
	}

	for (int i = low; i <= high; i++)
	{
		n = i;


		if (n == 0)
		{
			totalSize++;
			totalZero++;
			currentZeros++;
		}

		while (n > 0)
		{
			remainder = n % base;
			if (remainder < 10) //not A-Z
			{
				if (remainder == 0)
				{
					currentZeros++;
					totalZero++;
				}
				else //no zero, spree is over
				{
					if (consequentZero < currentZeros)
						consequentZero = currentZeros;
					currentZeros = 0;
				}
			}
			totalSize++;
			n /= base;
		}
	}
	printResult(mod, totalSize, totalZero, consequentZero);
}


int binarySpanAnalysis() {
	int low = 0;
	int high = 0;
	int mod = 0;
	int success = 1;
	int inputCheck = 0;
	char c;
	int base = 10;

	inputPrompt();
	while ( (inputCheck = scanf(" %c", &c)) && inputCheck != EOF ) {
	
		if (inputCheck == 1)
		{
			if (c == 'r')
			{
				inputCheck = scanf(" %d", &base);
				if (inputCheck == EOF)
					return 0;

				if (inputCheck != 1 || base < 2 || base  > 36 || scanf(" %c", &c) != 1 || c != ':' || scanf(" %c", &c) != 1 || c != '<')
				{
					invalidInput();
					return 0;
				}

			}
			else if (c != '<') // ye olde stuff
			{
				invalidInput();
				return 0;
			}


			inputCheck = scanf(" %d", &low);
			success &= inputCheck == 1 && low >= 0;

			inputCheck = scanf(" %c", &c);
			success &= inputCheck == 1 && c == ';';

			inputCheck = scanf(" %d", &high);
			success &= inputCheck == 1 && low <= high;

			inputCheck = scanf(" %c", &c);
			success &= inputCheck== 1 && c == '>';

			inputCheck = scanf(" %c", &c);
			success &= inputCheck == 1 && (c == 'l' || c == 's' || c == 'z');


			if (!success)
			{
				invalidInput();
				return 0;
			}

			if (c == 'l')
				mod = LENGTH;
			if (c == 's')
				mod = SEQUENCE;
			if (c == 'z')
				mod = ZEROS;

			parseArray(low, high, mod, base);
		}

	}

	return 1;
}

int main() {
	binarySpanAnalysis();
	return 0;
}
