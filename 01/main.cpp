#include <stdio.h>
#include <math.h>
#include <float.h>

int doublesEquality(double a, double b) {
	return (fabs(a-b) <= DBL_EPSILON * 10e6 * fabs(a+b));
}

void printInvalidInput() {
	printf("Nespravny vstup.\n");
}

void printError() {
	printf("Body netvori trojuhelnik.\n");
}

void inputPrompt(int a) {
	printf("Trojuhelnik #%d:\n", a);
}

void sortLengths( double * lengths ) {
	double tmp;

	if ( lengths[0] > lengths[1] )
	{
		tmp = lengths[0];
		lengths[0] = lengths[1];
		lengths[1] = tmp;
	}

	if ( lengths[0] > lengths[2] )
	{
		tmp = lengths[0];
		lengths[0] = lengths[2];
		lengths[2] = tmp;
	}

	if ( lengths[1] > lengths[2] )
	{
		tmp = lengths[1];
		lengths[1] = lengths[2];
		lengths[2] = tmp;
	}
}

//vector AB = (B-A) = (Bx-Ax, By-Ay)
double * vector(double * pointA, double * pointB) {
	double * vector = new double[2];
	vector[0] = pointB[0] - pointA[0];
	vector[1] = pointB[1] - pointA[1];
	return vector;
}

void inputPointPrompt(int a) {
	printf("Bod ");
	switch (a)
	{
	case 0:
		printf("A");
		break;
	case 1:
		printf("B");
		break;
	case 2:
		printf("C");
		break;
	}
	printf(":\n");
}

int inputPoint(double * point) {
	double x, y;
	int success = (scanf("%lf %lf",&x, &y) == 2);
	if (success)
	{
		point[0] = x;
		point[1] = y;
	 }

	return success;
}

double vectorLength(double * vector) {
	return sqrt(vector[0] * vector[0] + vector[1] * vector[1]);
}

int isCollinear( double * pointA, double * pointB, double * pointC ) {

	double a = pointA[1] - pointB[1];
	double b = pointA[0] - pointC[0];
	double c = pointA[1] - pointC[1];
	double d = pointA[0] - pointB[0];

	return doublesEquality(a*b, c*d);
}

//requires sorted array of 3 lengths, returns true if the triangles are equal
int areEqual(double * lengths1, double * lengths2)
{
	return  doublesEquality(lengths1[0], lengths2[0]) && doublesEquality(lengths1[1], lengths2[1]) && doublesEquality(lengths1[2], lengths1[2]);
}

double calculateLength(double * vectors[3])
{
	double circumference = 0;
	for (int i = 0; i < 3; i++)
		circumference += vectorLength(vectors[i]);

	return circumference;
}

int triangleEquations(double triangleA[][2], double triangleB[][2]) {

	double * vectorsB[3];
	double * vectorsA[3];
	double length1;
	double length2;
	int result;

	vectorsA[0] = vector(triangleA[0], triangleA[1]);
	vectorsA[1] = vector(triangleA[0], triangleA[2]);
	vectorsA[2] = vector(triangleA[1], triangleA[2]);
	vectorsB[0] = vector(triangleB[0], triangleB[1]);
	vectorsB[1] = vector(triangleB[0], triangleB[2]);
	vectorsB[2] = vector(triangleB[1], triangleB[2]);

	length1 = calculateLength(vectorsA);
	length2 = calculateLength(vectorsB);

	double vectorLengthsA[3];
	double vectorLengthsB[3];
	for (int i = 0; i < 3; i++)
	{
		vectorLengthsA[i] = vectorLength(vectorsA[i]);
		vectorLengthsB[i] = vectorLength(vectorsB[i]);
	}

	sortLengths(vectorLengthsA);
	sortLengths(vectorLengthsB);


	if (areEqual(vectorLengthsA, vectorLengthsB))
		result = 1;
	else if (doublesEquality(length1, length2))
		result = 2;
	//not equal, one is 'longer'
	else if (length1 > length2)
		result = 3;
	else
		result = 4;

	for (int i = 0; i < 3; i++)
	{
		delete vectorsA[i];
		delete vectorsB[i];
	}
	return result;
}

int main() {
	double triangleA[3][2], triangleB[3][2];

	int success;

	inputPrompt(1);
	for (int i = 0; i < 3; i++)
	{
		inputPointPrompt(i);
		success = inputPoint(triangleA[i]);
		if (!success)
		{
			printInvalidInput();
			return 0;
		}
	}

	if (isCollinear(triangleA[0], triangleA[1], triangleA[2]))
	{
		printError();
		return 0;
	}


	inputPrompt(2);
	for (int i = 0; i < 3; i++)
	{
		inputPointPrompt(i);
		success = inputPoint(triangleB[i]);
		if (!success)
		{
			printInvalidInput();
			return 0;
		}
	}

	if (isCollinear(triangleB[0], triangleB[1], triangleB[2]))
	{
		printError();
		return 0;
	}

	switch (triangleEquations(triangleA, triangleB))
	{
	case 1:
		printf("Trojuhelniky jsou shodne.\n");
		break;
	case 2:
		printf("Trojuhelniky nejsou shodne, ale maji stejny obvod.\n");
		break;
	case 3:
		printf("Trojuhelnik #1 ma vetsi obvod.\n");
		break;
	case 4:
		printf("Trojuhelnik #2 ma vetsi obvod.\n");
		break;
	}
	return 0;
}
