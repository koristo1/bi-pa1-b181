#ifndef __PROGTEST__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

typedef struct TEmployee {
    struct TEmployee *m_Next;
    struct TEmployee *m_Bak;
    char *m_Name;
} TEMPLOYEE;

#endif /* __PROGTEST__ */

void printList(TEMPLOYEE *src) {
    TEMPLOYEE *dummy = src;
    while (dummy != NULL) {
        printf("%s ", dummy->m_Name);
        dummy = dummy->m_Next;
    }

    printf("=====\n\n");
}

TEMPLOYEE *newEmployee(const char *name, TEMPLOYEE *next) {

    if (name == NULL)
        return NULL;

    TEMPLOYEE *dummy = (TEMPLOYEE *) malloc(sizeof(*dummy));

    dummy->m_Name = strdup(name);
    dummy->m_Next = next;
    dummy->m_Bak = NULL;

    return dummy;
}

TEMPLOYEE *cloneList(TEMPLOYEE *src) {

    //emptyList check
    if (src == NULL)
        return NULL;

    TEMPLOYEE *out = NULL;

    //single node check
    if (src->m_Next == NULL) {
        out = newEmployee(src->m_Name, NULL);
        out->m_Bak = (src->m_Bak == src ? out : NULL);
        return out;
    }

    TEMPLOYEE *currentNode = src;

    //place a copy of each node behind each respective node
    while (currentNode != NULL) {
        TEMPLOYEE *dummy = newEmployee(currentNode->m_Name, currentNode->m_Next);
        currentNode->m_Next = dummy;
        currentNode = currentNode->m_Next->m_Next;
    }

    currentNode = src;

    //for each original node, change the copied node's m_bak to the node behind the original node's m_bak
    while (currentNode != NULL) {
        if (currentNode->m_Bak != NULL)
            currentNode->m_Next->m_Bak = currentNode->m_Bak->m_Next;
        currentNode = currentNode->m_Next->m_Next;
    }

    //prepare the output list, starting on the first even node
    out = src->m_Next;
    currentNode = src;

    //split the list, all odds are the source nodes and evens are the outout nodes
    while (currentNode->m_Next != NULL) {
        TEMPLOYEE *dummy = currentNode->m_Next;
        currentNode->m_Next = currentNode->m_Next->m_Next;
        currentNode = dummy;
    }

    return out;
}

void freeList(TEMPLOYEE *src) {
    TEMPLOYEE *dummy;

    while (src != NULL) {
        dummy = src;
        src = src->m_Next;
        free(dummy->m_Name);
        free(dummy);
    }
}


#ifndef __PROGTEST__

int main(int argc,
         char *argv[]) {
    TEMPLOYEE *a, *b;
    char dummy[100];

    assert (sizeof(TEMPLOYEE) == 3 * sizeof(void *));
    a = NULL;
    a = newEmployee("Peter", a);
    a = newEmployee("John", a);
    a = newEmployee("Joe", a);
    a = newEmployee("Maria", a);
    a->m_Bak = a->m_Next;
    a->m_Next->m_Next->m_Bak = a->m_Next->m_Next->m_Next;
    a->m_Next->m_Next->m_Next->m_Bak = a->m_Next;
    assert (a
            && !strcmp(a->m_Name, "Maria")
            && a->m_Bak == a->m_Next);
    assert (a->m_Next
            && !strcmp(a->m_Next->m_Name, "Joe")
            && a->m_Next->m_Bak == NULL);
    assert (a->m_Next->m_Next
            && !strcmp(a->m_Next->m_Next->m_Name, "John")
            && a->m_Next->m_Next->m_Bak == a->m_Next->m_Next->m_Next);
    assert (a->m_Next->m_Next->m_Next
            && !strcmp(a->m_Next->m_Next->m_Next->m_Name, "Peter")
            && a->m_Next->m_Next->m_Next->m_Bak == a->m_Next);
    assert (a->m_Next->m_Next->m_Next->m_Next == NULL);

    b = cloneList(a);

    strncpy(dummy, "Moe", sizeof(dummy));
    a = newEmployee(dummy, a);
    strncpy(dummy, "Victoria", sizeof(dummy));
    a = newEmployee(dummy, a);
    strncpy(dummy, "Peter", sizeof(dummy));
    a = newEmployee(dummy, a);
    b->m_Next->m_Next->m_Next->m_Bak = b->m_Next->m_Next;
    assert (a
            && !strcmp(a->m_Name, "Peter")
            && a->m_Bak == NULL);
    assert (a->m_Next
            && !strcmp(a->m_Next->m_Name, "Victoria")
            && a->m_Next->m_Bak == NULL);
    assert (a->m_Next->m_Next
            && !strcmp(a->m_Next->m_Next->m_Name, "Moe")
            && a->m_Next->m_Next->m_Bak == NULL);
    assert (a->m_Next->m_Next->m_Next
            && !strcmp(a->m_Next->m_Next->m_Next->m_Name, "Maria")
            && a->m_Next->m_Next->m_Next->m_Bak == a->m_Next->m_Next->m_Next->m_Next);
    assert (a->m_Next->m_Next->m_Next->m_Next
            && !strcmp(a->m_Next->m_Next->m_Next->m_Next->m_Name, "Joe")
            && a->m_Next->m_Next->m_Next->m_Next->m_Bak == NULL);
    assert (a->m_Next->m_Next->m_Next->m_Next->m_Next
            && !strcmp(a->m_Next->m_Next->m_Next->m_Next->m_Next->m_Name, "John")
            && a->m_Next->m_Next->m_Next->m_Next->m_Next->m_Bak == a->m_Next->m_Next->m_Next->m_Next->m_Next->m_Next);
    assert (a->m_Next->m_Next->m_Next->m_Next->m_Next->m_Next
            && !strcmp(a->m_Next->m_Next->m_Next->m_Next->m_Next->m_Next->m_Name, "Peter")
            && a->m_Next->m_Next->m_Next->m_Next->m_Next->m_Next->m_Bak == a->m_Next->m_Next->m_Next->m_Next);
    assert (a->m_Next->m_Next->m_Next->m_Next->m_Next->m_Next->m_Next == NULL);
    assert (b
            && !strcmp(b->m_Name, "Maria")
            && b->m_Bak == b->m_Next);
    assert (b->m_Next
            && !strcmp(b->m_Next->m_Name, "Joe")
            && b->m_Next->m_Bak == NULL);
    assert (b->m_Next->m_Next
            && !strcmp(b->m_Next->m_Next->m_Name, "John")
            && b->m_Next->m_Next->m_Bak == b->m_Next->m_Next->m_Next);
    assert (b->m_Next->m_Next->m_Next
            && !strcmp(b->m_Next->m_Next->m_Next->m_Name, "Peter")
            && b->m_Next->m_Next->m_Next->m_Bak == b->m_Next->m_Next);
    assert (b->m_Next->m_Next->m_Next->m_Next == NULL);
    freeList(a);
    b->m_Next->m_Bak = b->m_Next;
    a = cloneList(b);
    assert (a
            && !strcmp(a->m_Name, "Maria")
            && a->m_Bak == a->m_Next);
    assert (a->m_Next
            && !strcmp(a->m_Next->m_Name, "Joe")
            && a->m_Next->m_Bak == a->m_Next);
    assert (a->m_Next->m_Next
            && !strcmp(a->m_Next->m_Next->m_Name, "John")
            && a->m_Next->m_Next->m_Bak == a->m_Next->m_Next->m_Next);
    assert (a->m_Next->m_Next->m_Next
            && !strcmp(a->m_Next->m_Next->m_Next->m_Name, "Peter")
            && a->m_Next->m_Next->m_Next->m_Bak == a->m_Next->m_Next);
    assert (a->m_Next->m_Next->m_Next->m_Next == NULL);
    assert (b
            && !strcmp(b->m_Name, "Maria")
            && b->m_Bak == b->m_Next);
    assert (b->m_Next
            && !strcmp(b->m_Next->m_Name, "Joe")
            && b->m_Next->m_Bak == b->m_Next);
    assert (b->m_Next->m_Next
            && !strcmp(b->m_Next->m_Next->m_Name, "John")
            && b->m_Next->m_Next->m_Bak == b->m_Next->m_Next->m_Next);
    assert (b->m_Next->m_Next->m_Next
            && !strcmp(b->m_Next->m_Next->m_Next->m_Name, "Peter")
            && b->m_Next->m_Next->m_Next->m_Bak == b->m_Next->m_Next);
    assert (b->m_Next->m_Next->m_Next->m_Next == NULL);
    freeList(b);
    freeList(a);
    return 0;
}

#endif /* __PROGTEST__ */