#ifndef __PROGTEST__
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <assert.h>
#endif /* __PROGTEST__ */

#define  UINT unsigned int
#define  ULONG unsigned long long int

UINT combination(UINT * segmentCounter, ULONG currentLength, ULONG maximalLength, ULONG segment)
{
	ULONG remainingLength = maximalLength - currentLength;
	if (remainingLength % segment != 0)
		return 0;

	*segmentCounter = remainingLength / segment;
	return 1;
}

ULONG fullCombination(ULONG unit1, ULONG unit2, ULONG maximalS1Count, ULONG currentLength,ULONG maximalLength, UINT * c1, UINT * c2) {
	
	UINT segmentOneCounter = 0;
	UINT segmentTwoCounter = 0;
	ULONG validCombinations = 0;
	ULONG isValid = 0;

	//non-zero non-equal segment lengths
	while (segmentOneCounter <= maximalS1Count)
	{
		isValid = combination(&segmentTwoCounter, currentLength + segmentOneCounter * unit1, maximalLength, unit2);
		if (isValid)
		{
			*c1 = segmentOneCounter;
			*c2 = segmentTwoCounter;
		}
		validCombinations += isValid; //can move counter++ to this and 'simplify' the loop
		segmentOneCounter++;
	}

	return validCombinations;
}

int segmentsAreZero(UINT a, UINT b) {
	return a == 0 && b == 0;
}


unsigned long long int hyperloop(unsigned long long int length, unsigned int s1, unsigned int s2, unsigned int bulkhead, unsigned int * c1,	unsigned int * c2)
{
	if (segmentsAreZero(s1,s2) && length != bulkhead)
		return 0;

	if (segmentsAreZero(s1, s2) && length == bulkhead)
		return 1;

	ULONG validCombinations = 0;
	// pairs of bulkhead and respective segments
	ULONG unit1 = bulkhead + s1; 
	ULONG unit2 = bulkhead + s2; 
	UINT unit2Counter = 0;

	if (s1 == 0 && s2 != 0) // one non-zero
	{
		validCombinations += combination(&unit2Counter, bulkhead, length, unit2);
		if (validCombinations != 0) //found the one and only combination
		{
			*c1 = 0; // no units of first segment used
			*c2 = unit2Counter;
		}
		return validCombinations;
	}

	if ((s2 == 0 && s1 != 0) || s2 == s1) // one non-zero or two equal values, at which point one might as well be 0
	{
		validCombinations += combination(&unit2Counter, bulkhead, length, unit1);
		if (validCombinations != 0) //found the one and only combination
		{
			*c2 = 0; // no units of first segment used
			*c1 = unit2Counter;
		}
		return validCombinations;
	}

	return unit1 > unit2 ? fullCombination(unit1, unit2, length / unit1, bulkhead, length, c1, c2) : fullCombination(unit2, unit1, length / unit2, bulkhead, length, c2, c1);
}

#ifndef __PROGTEST__
int main(int argc, char * argv[])
{
	unsigned int c1, c2;
	assert(hyperloop(100, 4, 7, 0, &c1, &c2) == 4
		&& 4 * c1 + 7 * c2 + 0 * (c1 + c2 + 1) == 100);
	assert(hyperloop(123456, 8, 3, 3, &c1, &c2) == 1871
		&& 8 * c1 + 3 * c2 + 3 * (c1 + c2 + 1) == 123456);
	assert(hyperloop(127, 12, 8, 0, &c1, &c2) == 0);
	assert(hyperloop(127, 12, 4, 3, &c1, &c2) == 1
		&& 12 * c1 + 4 * c2 + 3 * (c1 + c2 + 1) == 127);
	assert(hyperloop(100, 35, 0, 10, &c1, &c2) == 1
		&& c2 == 0
		&& 35 * c1 + 10 * (c1 + 1) == 100);
	assert(hyperloop(110, 30, 30, 5, &c1, &c2) == 1
		&& 30 * c1 + 30 * c2 + 5 * (c1 + c2 + 1) == 110);
	c1 = 2;
	c2 = 7;
	assert(hyperloop(110, 30, 30, 0, &c1, &c2) == 0 && c1 == 2 && c2 == 7);
	c1 = 4;
	c2 = 8;
	assert(hyperloop(9, 1, 2, 10, &c1, &c2) == 0 && c1 == 4 && c2 == 8);
	assert(hyperloop(916000, 1, 2, 1, &c1, &c2) == 152667
		&& 1 * c1 + 2 * c2 + 1 * (c1 + c2 + 1) == 916000);
	c1 = 1;
	c2 = 2;
	assert(hyperloop(ULLONG_MAX, 0, 0, 0, &c1, &c2) == 0
		&& c1 == 1 && c2 == 2);
	assert(hyperloop(ULLONG_MAX, 0, 0, 24, &c1, &c2) == 0
		&& c1 == 1 && c2 == 2);
	assert(hyperloop(1000, 1, 0, 0, &c1, &c2) == 1
		&& c1 == 1000 && c2 == 0);
	assert(hyperloop(667, 0, 1, 1, &c1, &c2) == 1
		&& c1 == 0 && c2 == 333);
	assert(hyperloop(635, 0, 0, 635, &c1, &c2) == 1);
	return 0;
}
#endif /* __PROGTEST__ */
